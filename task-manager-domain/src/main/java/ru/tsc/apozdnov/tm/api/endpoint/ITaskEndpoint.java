package ru.tsc.apozdnov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.dto.request.*;
import ru.tsc.apozdnov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ITaskEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, ITaskEndpoint.class);
    }

    @NotNull
    @WebMethod
    TaskChangeStatusByIdResponse changeTaskStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskClearResponse clearTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    );

    @NotNull
    @WebMethod
    TaskCreateResponse createTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    );

    @NotNull
    @WebMethod
    TaskListResponse listTask(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    );

    @NotNull
    @WebMethod
    TaskListByProjectIdResponse listTaskByProjectId(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    );

    @NotNull
    @WebMethod
    TaskRemoveByIdResponse removeTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskShowByIdResponse showTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskShowByIdRequest request
    );

    @NotNull
    @WebMethod
    TaskUpdateByIdResponse updateTaskById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    );

}