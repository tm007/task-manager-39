//package ru.tsc.apozdnov.tm.repository;
//
//import org.jetbrains.annotations.NotNull;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//
//import ru.tsc.apozdnov.tm.api.repository.ITaskRepository;
//import ru.tsc.apozdnov.tm.model.Task;
//
//import java.util.List;
//import java.util.UUID;
//
//public class TaskRepositoryTest {
//
//    @NotNull
//    private static final String USER_ID_1 = UUID.randomUUID().toString();
//
//    @NotNull
//    private static final String USER_ID_2 = UUID.randomUUID().toString();
//
//    private static long INITIAL_SIZE;
//
//    @NotNull
//    private final ITaskRepository taskRepository = new TaskRepository();
//
//    @Before
//    public void init() {
//        taskRepository.create(USER_ID_1, "test-1");
//        taskRepository.create(USER_ID_1, "test-2");
//        taskRepository.create(USER_ID_2, "test-3");
//        INITIAL_SIZE = taskRepository.getSize();
//    }
//
//    @Test
//    public void create() {
//        taskRepository.create(USER_ID_1, "test");
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        taskRepository.create(USER_ID_1, "test", "description");
//        Assert.assertEquals(INITIAL_SIZE + 2, taskRepository.getSize());
//    }
//
//    @Test
//    public void add() {
//        @NotNull final Task task = new Task();
//        taskRepository.add(USER_ID_1, task);
//        Assert.assertEquals(INITIAL_SIZE + 1, taskRepository.getSize());
//        Assert.assertTrue(taskRepository.findAll().contains(task));
//    }
//
//    @Test
//    public void clear() {
//        taskRepository.clear();
//        Assert.assertEquals(0, taskRepository.getSize());
//    }
//
//    @Test
//    public void findAll() {
//        @NotNull final List<Task> tasksAll = taskRepository.findAll();
//        Assert.assertEquals(INITIAL_SIZE, tasksAll.size());
//        @NotNull final List<Task> tasksOwnedUser1 = taskRepository.findAll(USER_ID_1);
//        Assert.assertEquals(2, tasksOwnedUser1.size());
//        @NotNull final List<Task> tasksOwnedUser3 = taskRepository.findAll(UUID.randomUUID().toString());
//        Assert.assertEquals(0, tasksOwnedUser3.size());
//    }
//
//    @Test
//    public void findOneById() {
//        @NotNull final String taskName = "test find by id";
//        @NotNull final Task task = taskRepository.create(USER_ID_1, taskName);
//        @NotNull final String taskId = task.getId();
//        Assert.assertNotNull(taskRepository.findOneById(taskId));
//        Assert.assertEquals(taskName, taskRepository.findOneById(taskId).getName());
//        Assert.assertNull(taskRepository.findOneById(UUID.randomUUID().toString()));
//        Assert.assertNotNull(taskRepository.findOneById(USER_ID_1, taskId));
//        Assert.assertEquals(taskName, taskRepository.findOneById(USER_ID_1, taskId).getName());
//        Assert.assertNull(taskRepository.findOneById(USER_ID_1, UUID.randomUUID().toString()));
//    }
//
//    @Test
//    public void findOneByIndex() {
//        @NotNull final String taskName = "test find by index";
//        taskRepository.create(USER_ID_1, taskName);
//        @NotNull final Integer taskIndex = 2;
//        Assert.assertNotNull(taskRepository.findOneByIndex(USER_ID_1, taskIndex));
//        Assert.assertEquals(taskName, taskRepository.findOneByIndex(USER_ID_1, taskIndex).getName());
//    }
//
//    @Test
//    public void existsById() {
//        @NotNull final String taskName = "test exist by id";
//        @NotNull final Task task = taskRepository.create(USER_ID_1, taskName);
//        @NotNull final String taskId = task.getId();
//        Assert.assertTrue(taskRepository.existsById(taskId));
//        Assert.assertFalse(taskRepository.existsById(UUID.randomUUID().toString()));
//    }
//
//    @Test
//    public void remove() {
//        @NotNull final Task task = taskRepository.create(USER_ID_1, "test");
//        @NotNull final String taskId = task.getId();
//        taskRepository.remove(task);
//        Assert.assertNull(taskRepository.findOneById(taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//        taskRepository.add(task);
//        taskRepository.remove(USER_ID_1, task);
//        Assert.assertNull(taskRepository.findOneById(USER_ID_1, taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//    }
//
//    @Test
//    public void removeById() {
//        @NotNull final Task task = taskRepository.create(USER_ID_1, "test");
//        @NotNull final String taskId = task.getId();
//        taskRepository.removeById(taskId);
//        Assert.assertNull(taskRepository.findOneById(taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//        taskRepository.add(task);
//        taskRepository.removeById(USER_ID_1, taskId);
//        Assert.assertNull(taskRepository.findOneById(USER_ID_1, taskId));
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//    }
//
//    @Test
//    public void removeByIndex() {
//        taskRepository.create(USER_ID_1, "test");
//        @NotNull final Integer taskIndex = 2;
//        taskRepository.removeByIndex(USER_ID_1, taskIndex);
//        Assert.assertEquals(INITIAL_SIZE, taskRepository.getSize());
//    }
//
//}