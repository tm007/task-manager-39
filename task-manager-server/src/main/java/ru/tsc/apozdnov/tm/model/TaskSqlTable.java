package ru.tsc.apozdnov.tm.model;

import org.mybatis.dynamic.sql.SqlColumn;
import org.mybatis.dynamic.sql.SqlTable;
import ru.tsc.apozdnov.tm.enumerated.Status;

import java.sql.JDBCType;
import java.util.Date;

public class TaskSqlTable extends SqlTable {

    public final SqlColumn<String> id = column("id", JDBCType.VARCHAR);

    public final SqlColumn<String> userId = column("user_id", JDBCType.VARCHAR);

    public final SqlColumn<String> name = column("name", JDBCType.VARCHAR);

    public final SqlColumn<String> description = column("description", JDBCType.VARCHAR);

    public final SqlColumn<String> projectId = column("project_id", JDBCType.VARCHAR);

    public final SqlColumn<Status> status = column("status", JDBCType.VARCHAR);

    public final SqlColumn<Date> created = column("created", JDBCType.TIMESTAMP);

    public final SqlColumn<Date> dateBegin = column("date_begin", JDBCType.TIMESTAMP);

    public final SqlColumn<Date> dateEnd = column("date_end", JDBCType.TIMESTAMP);

    public TaskSqlTable() {
        super("tm_task");
    }

}