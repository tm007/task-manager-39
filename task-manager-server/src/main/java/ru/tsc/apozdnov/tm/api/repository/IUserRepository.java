package ru.tsc.apozdnov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.apozdnov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, first_name, last_name, middle_name, role, lock)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{lock})")
    void add(@NotNull User user);

    @Insert("INSERT INTO tm_user (id, login, password_hash, email, first_name, last_name, middle_name, role, lock)"
            + "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, #{lock})")
    void addAll(@NotNull Collection<User> users);

    @Update("UPDATE tm_user SET login = #{login}, password_hash = #{passwordHash}, email = #{email}, first_name = #{firstName},"
            + "last_name = #{lastName}, middle_name = #{middleName}, role = #{role}, locked = #{lock} "
            + "WHERE id = #{id}")
    void update(@NotNull User user);

    @Override
    @Delete("DELETE FROM tm_user")
    void clear();

    @Override
    @NotNull
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, lock FROM tm_user")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    List<User> findAll();

    @Override
    @Select("SELECT count(1) = 1 FROM tm_user WHERE id = #{id}")
    boolean existsById(@Param("id") @NotNull String id);

    @Override
    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, lock FROM tm_user "
            + "WHERE id = #{id}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findOneById(@Param("id") @NotNull String id);

    @Override
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void remove(@NotNull User user);

    @Override
    @Delete("DELETE FROM tm_user WHERE id = #{id}")
    void removeById(@Param("id") @NotNull String id);

    @Override
    @Select("SELECT count(1) FROM tm_user")
    long getCount();

    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, lock FROM tm_user "
            + "WHERE login = #{login}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findOneByLogin(@Param("login") @NotNull String login);

    @Nullable
    @Select("SELECT id, login, password_hash, email, first_name, last_name, middle_name, role, lock FROM tm_user "
            + "WHERE email = #{email}")
    @Results(value = {
            @Result(property = "passwordHash", column = "password_hash"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "middleName", column = "middle_name")
    })
    User findOneByEmail(@Param("email") @NotNull String email);

}