package ru.tsc.apozdnov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.tsc.apozdnov.tm.api.repository.IUserOwnedRepository;
import ru.tsc.apozdnov.tm.api.service.IConnectionService;
import ru.tsc.apozdnov.tm.api.service.IUserOwnedService;
import ru.tsc.apozdnov.tm.model.AbstractUserOwnedModel;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

}